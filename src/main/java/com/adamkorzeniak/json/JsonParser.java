package com.adamkorzeniak.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class JsonParser {

    private final ObjectMapper objectMapper;

    public JsonParser() {
        this(new ObjectMapper());
    }

    public JsonParser(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T fromJson(String json, Class<T> classOfT) throws JsonProcessingException {
        return objectMapper.readValue(json, classOfT);
    }

    public Map<String, Object> fromJsonToMap(String json) throws JsonProcessingException {
        TypeReference<Map<String,Object>> typeRef = new TypeReference<>() {};
        return objectMapper.readValue(json, typeRef);
    }
}
