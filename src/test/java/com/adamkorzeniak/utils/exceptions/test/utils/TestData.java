package com.adamkorzeniak.utils.exceptions.test.utils;

public class TestData {
    public static final String DUMMY_PERSON_OBJECT_NAME = "dummyPerson";
    public static final String NAME_FIELD = "name";
    public static final String AGE_FIELD = "age";
}
