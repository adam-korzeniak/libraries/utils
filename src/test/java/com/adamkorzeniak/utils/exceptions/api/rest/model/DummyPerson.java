package com.adamkorzeniak.utils.exceptions.api.rest.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DummyPerson {

    @NotBlank
    private String name;

    @NotNull
    private Integer age;
}
