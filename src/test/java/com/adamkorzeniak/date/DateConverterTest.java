package com.adamkorzeniak.date;

import com.adamkorzeniak.file.FileReader;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DateConverterTest {

    @Test
    void ToLocalDateTime_IsNull_ReturnsNull() {
        //given
        OffsetDateTime input = null;
        //when
        LocalDateTime result = DateConverter.toLocalDateTime(input);
        //then
        assertThat(result)
                .isNull();
    }

    @Test
    void ToLocalDateTime_IsDate_ReturnsDate() {
        //given
        OffsetDateTime input = OffsetDateTime.now();
        //when
        LocalDateTime result = DateConverter.toLocalDateTime(input);
        //then
        assertThat(result)
                .isEqualTo(input.atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
    }

    @Test
    void ToOffsetDateTime_IsNull_ReturnsNull() {
        //given
        LocalDateTime input = null;
        //when
        OffsetDateTime result = DateConverter.toOffsetDateTime(input);
        //then
        assertThat(result)
                .isNull();
    }

    @Test
    void ToOffsetDateTime_IsDate_ReturnsDate() {
        //given
        LocalDateTime input = LocalDateTime.now();
        //when
        OffsetDateTime result = DateConverter.toOffsetDateTime(input);
        //then
        assertThat(result)
                .isEqualTo(input.atZone(ZoneId.systemDefault()).toOffsetDateTime());
    }
}